﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace RaceGameExample {
    class Car {
        private Point position;
        private float rotation;
        private double speed;
        private bool leftPressed = false, rightPressed = false, throttlePressed = false, brakePressed = false;
        private Keys leftKey, rightKey, throttleKey, brakeKey;
        private Image image;
        private int health;
        private int fuel;

        /// <summary>
        /// Constructor of the car class
        /// </summary>
        /// <param name="postionx">starting position of the vehicle (Horiz)</param>
        /// <param name="positiony">starting position of the vehicle (Vert)</param>
        /// <param name="rotation">starting rotation of the vehicle (0 for vehicle is pointing left)</param>
        /// <param name="speed">starting speed of the vehicle</param>
        /// <param name="spd">starting speed of the vehicle</param>
        /// <param name="hp">starting health of the vehicle</param>
        /// <param name="leftKey">the key to steer left</param>
        /// <param name="rightKey">the key to steer right</param>
        /// <param name="throttleKey">the key to throttle</param>
        /// <param name="brakeKey">the key to brake/reverse</param>
        /// <param name="image">the image used to draw the vehicle</param>
        public Car(int positionx, int positiony, float rotation, double speed, int hp, int fu, Keys leftKey, Keys rightKey, Keys throttleKey, Keys brakeKey, Image image)
        {
            position.X = positionx;
            position.Y = positiony;
            this.rotation = rotation;
            this.speed = speed;
            this.health = hp;
            this.fuel = fu;
            this.leftKey = leftKey;
            this.rightKey = rightKey;
            this.throttleKey = throttleKey;
            this.brakeKey = brakeKey;
            this.image = image;
        }

        public void handleKeyDownEvent(KeyEventArgs keys) 
        {
            if (leftKey == keys.KeyCode)
                leftPressed = true;
            if (rightKey == keys.KeyCode)
                rightPressed = true;
            if (throttleKey == keys.KeyCode)
                throttlePressed = true;
            if (brakeKey == keys.KeyCode)
                brakePressed = true;
        }

        public void handleKeyUpEvent(KeyEventArgs keys) 
        {
            if (leftKey == keys.KeyCode)
                leftPressed = false;
            if (rightKey == keys.KeyCode)
                rightPressed = false;
            if (throttleKey == keys.KeyCode)
                throttlePressed = false;
            if (brakeKey == keys.KeyCode)
                brakePressed = false;
        }

        public Point getPosition() 
        {
            return position;
        }

        public void setPosition(int posx, int posy)
        {
            position.X = posx;
            position.Y = posy;
        }

        public Image getImage() 
        {
            return image;
        }

        private void accelerate() 
        {
            speed = speed + .1;

            if (speed >= 5.0)
                speed = 5.0;
        }

        private void brake() 
        {
            speed = speed - .1;

            if (speed <= -2.0)
                speed = -2.0;
        }

        private void coast()
        {
            if (speed >= .02)
                speed -= .05;
            else if (speed <= -.02)
                speed += 0.05;
            else
                speed = 0;
        }

        private void rotateRight()
        {
            if (speed != 0)
                this.rotation += .07f;
        }

        private void rotateLeft()
        {
            if (speed != 0)
                this.rotation -= .07f;
        }

        private void changeSpeed()
        {
            if (throttlePressed)
                accelerate();
            else if (brakePressed)
                brake();
            else
                coast();
            
            if (leftPressed)
                rotateLeft();
            else if (rightPressed)
                rotateRight();
        }

        /// <summary>
        /// Calculates the new position for the car
        /// </summary>
        public void calculateNewPosition()
        {
            changeSpeed();
            position.X += (int)Math.Round(speed * Math.Cos(rotation)); //pure magic here!
            position.Y += (int)Math.Round(speed * Math.Sin(rotation)); //more magic here
        }
    }
}
