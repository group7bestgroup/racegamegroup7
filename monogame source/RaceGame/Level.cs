﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    public abstract class Level
    {
        /// <summary>
        /// gets called when the level is initialized
        /// </summary>
        /// <param name="content"></param>
        public abstract void Start(ContentManager content);
        /// <summary>
        /// gets called every frame
        /// </summary>
        /// <param name="delta"></param>
        public abstract void Update(float delta);
        /// <summary>
        /// used to draw sprites
        /// </summary>
        /// <param name="spriteBatch"></param>
        public abstract void Draw(SpriteBatch spriteBatch);
    }
}
