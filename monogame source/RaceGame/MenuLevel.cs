﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    class MenuLevel : Level
    {

        private Game1 gameManager;
        private Texture2D textureStart;
        private Texture2D textureExit;
        private Texture2D textureBackground;

        public MenuLevel(Game1 gameManager)
        {
            this.gameManager = gameManager;
        }

        public override void Start(ContentManager content)
        {
            textureStart = content.Load<Texture2D>("Assets/Images/Menu/StartButton");
            textureExit = content.Load<Texture2D>("Assets/Images/Menu/ExitButton");
            textureBackground = content.Load<Texture2D>("Assets/Images/Menu/menuBackground");

            //sets the position for the menu buttons
            Vector2 posStart = new Vector2((gameManager.graphics.PreferredBackBufferWidth / 2f) - (textureStart.Width / 2f) - 350, 200);
            Vector2 posExit = new Vector2((gameManager.graphics.PreferredBackBufferWidth / 2f) - (textureExit.Width / 2f) - 350, 280);

            gameManager.GameObjects.Add(new StartButton(textureStart, posStart, gameManager));
            gameManager.GameObjects.Add(new ExitButton(textureExit, posExit, gameManager));

            //loops through all the gameobjects and calls their start function
            foreach (GameObject gameObject in gameManager.GameObjects)
            {
                gameObject.Start(content);
            }

        }

        public override void Update(float delta)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //draws the background
            spriteBatch.Draw(textureBackground, new Rectangle(0, 0, 1024, 768),
                                                null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
        }
    }
}
