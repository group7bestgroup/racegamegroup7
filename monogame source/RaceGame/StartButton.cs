﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RaceGame
{
    class StartButton : GameObject
    {

        private Color color = Color.White;
        private Game1 gameManager;

        private int playerCount = 2;

        public StartButton(Texture2D sprite, Vector2 position, Game1 gm)
        {
            this.sprite = sprite;
            this.position = position;
            gameManager = gm;
        }

        public override void Start(ContentManager content)
        {
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");
        }

        public override void Update(float delta)
        {
            //if the mouse is within the collision box then make the text red
            if (GetBoundingBox().Contains(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
            {
                //if the user presses he left mouse butten load the next level
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    gameManager.nextLevel = new RaceLevel(playerCount, gameManager);
                }

                color = Color.Red;
            }
            else
            {
                color = Color.White;
            }
        }

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)sprite.Width, (int)sprite.Height);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, GetBoundingBox(), null, color, rotation, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        public override void OnCollisionEnter(GameObject other)
        {
        }
    }
}
