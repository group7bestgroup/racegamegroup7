﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace RaceGame
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public GraphicsDeviceManager graphics;
        //readonly vars
        SpriteBatch spriteBatch;

        private FrameCounter frameCounter = new FrameCounter();
        private KeyboardState oldState;
        public SpriteFont defaultFont;
        public SpriteFont PixelFont;
        public Level currentLevel;
        public Level nextLevel;

        #region pauzemenu
        public bool gamePauzed = false;
        private Rectangle resumePos;
        private Rectangle backPos;
        private Texture2D textureResume;
        private Texture2D textureBackMenu;
        private Color resumeColor = Color.White;
        private Color backColor = Color.White;
        #endregion


        string imagePath = "Assets/Images/";

        //keys of players going clockwise up > right > down > left and an actionkey at the end.
        public Keys[,] playerKeys = new Keys[4, 5] {
                                              {
                                                  Keys.W, Keys.D, Keys.S, Keys.A, Keys.E
                                              },
                                              {
                                                  Keys.Up, Keys.Right, Keys.Down, Keys.Left, Keys.Space
                                              },
                                              {
                                                  Keys.I, Keys.L, Keys.K, Keys.J, Keys.O
                                              },
                                              {
                                                  Keys.NumPad8, Keys.NumPad6, Keys.NumPad5, Keys.NumPad4, Keys.NumPad9
                                              }

                                            };


        //lists the current vehicles
        Vehicle[] vehicles;
        public List<GameObject> GameObjects = new List<GameObject>();
        private List<GameObject> newGameObject = new List<GameObject>();
        public List<Texture2D> PowerUpSprites = new List<Texture2D>();
        public List<GameObject> CheckpointList = new List<GameObject>();

        public Texture2D defaultBoundingBox;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.IsMouseVisible = true;

            graphics.PreferredBackBufferHeight = 768;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.ApplyChanges();

            var form = (System.Windows.Forms.Form)System.Windows.Forms.Control.FromHandle(this.Window.Handle);
            form.Location = new System.Drawing.Point((GraphicsDevice.DisplayMode.Width / 2) - (graphics.PreferredBackBufferWidth / 2), (GraphicsDevice.DisplayMode.Height / 2) - (graphics.PreferredBackBufferHeight / 2));
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            defaultFont = Content.Load<SpriteFont>("Assets/fonts/defaultFont");
            PixelFont = Content.Load<SpriteFont>("Assets/fonts/8bitFont");
            defaultBoundingBox = Content.Load<Texture2D>("Assets/Images/Terrain/testWall");
            textureResume = Content.Load<Texture2D>("Assets/Images/Menu/ResumeButton");
            textureBackMenu = Content.Load<Texture2D>("Assets/Images/Menu/BackMenuButton");

            resumePos = new Rectangle((int)(graphics.PreferredBackBufferWidth / 2f) - (int)(textureResume.Width / 2f), 200, textureResume.Width, textureResume.Height);
            backPos = new Rectangle((int)(graphics.PreferredBackBufferWidth / 2f) - (int)(textureBackMenu.Width / 2f), 280, textureBackMenu.Width, textureBackMenu.Height);


            currentLevel = new MenuLevel(this);
            currentLevel.Start(Content);
            nextLevel = currentLevel;

            //adding the sprites from the powerupsprites
            for (int i = 0; i < 8; i++)
            {
                PowerUpSprites.Add(Content.Load<Texture2D>("Assets/Images/Powerups/" + i));
            }


            foreach (GameObject gameObject in GameObjects)
            {
                gameObject.Start(Content);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

            currentLevel.Update(delta);


            if (!gamePauzed)
            {
                foreach (GameObject gameObject in GameObjects)
                {
                    gameObject.Update(delta);
                }

                //adds every new gameobject to the list of gameobjects
                foreach (var gameObject in newGameObject)
                {
                    GameObjects.Add(gameObject);
                }
                newGameObject.Clear();

                if (currentLevel != nextLevel)
                {
                    GameObjects.Clear();
                    newGameObject.Clear();
                    currentLevel = nextLevel;
                    currentLevel.Start(Content);
                }
            }
            else
            {
                if (resumePos.Contains(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
                {
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        gamePauzed = false;
                    }
                    resumeColor = Color.Red;

                }
                else
                {
                    resumeColor = Color.White;
                }
                if (backPos.Contains(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
                {
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        nextLevel = new MenuLevel(this);
                        gamePauzed = false;
                    }
                    backColor = Color.Red;

                }
                else
                {
                    backColor = Color.White;
                }
            }
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            currentLevel.Draw(spriteBatch);

            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            frameCounter.Update(deltaTime);
            var fps = string.Format("FPS: {0}", frameCounter.AverageFramesPerSecond);

            spriteBatch.DrawString(defaultFont, fps, new Vector2(1, 1), Color.Black);

            spriteBatch.DrawString(defaultFont, "mouse x:" + Mouse.GetState().X + "\n mouse y: " + Mouse.GetState().Y, new Vector2(1, 40), Color.Black);

            foreach (var gameObject in GameObjects)
            {
                gameObject.Draw(spriteBatch);
                //gameObject.DebugDraw(spriteBatch);
            }

            if (gamePauzed)
            {
                spriteBatch.Draw(textureResume, resumePos, null, resumeColor, 0, new Vector2(0, 0), SpriteEffects.None, 1);
                spriteBatch.Draw(textureBackMenu, backPos, null, backColor, 0, new Vector2(0, 0), SpriteEffects.None, 1);
            }


            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void CreateGameObject(GameObject gameObject)
        {
            newGameObject.Add(gameObject);
        }

    }
}
