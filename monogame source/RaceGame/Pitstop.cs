﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    /// <summary>
    /// cointains the behaviour of the pitstop
    /// </summary>
    class Pitstop : GameObject
    {
        private Vector2 size;

        /// <summary>
        /// creates a new pitstop
        /// </summary>
        /// <param name="pos">position of the pitstop</param>
        /// <param name="rot">rotation of pitstop</param>
        /// <param name="size">size of pitstop</param>
        public Pitstop(Vector2 pos, float rot, Vector2 size)
        {
            this.position = pos;
            this.rotation = rot;
            this.size = size;
        }

        //start behaviour
        public override void Start(ContentManager content)
        {
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");
            sprite = content.Load<Texture2D>("Assets/Images/Terrain/checkpointSprite");
        }

        //drawing behaviour
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, new Rectangle(Convert.ToInt32(position.X), Convert.ToInt32(position.Y), Convert.ToInt32(size.X), Convert.ToInt32(size.Y)),
                                                            null, Color.White, rotation, new Vector2(sprite.Width / 2, sprite.Height / 2), SpriteEffects.None, 1);
        }

        //debug drawing behaviour only if debugdraw is enabled
        public override void DebugDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(boundingSprite, GetBoundingBox(), null, Color.Blue, 0, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        //update behaviour
        public override void Update(float delta){}

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        /// <summary>
        /// Checks collision with this object
        /// </summary>
        /// <param name="other">object colliding with</param>
        public override void OnCollisionEnter(GameObject other)
        {
            if (other.GetType() == typeof (Vehicle))
            {
                Vehicle otherVehicle = (Vehicle) other;

                if ((otherVehicle.GetHealth() != otherVehicle.GetMaxHealth() ||
                    otherVehicle.GetFuel() != otherVehicle.GetMaxFuel()) && !otherVehicle.GetPassedPitstop())
                {
                    otherVehicle.ExecutePitstop();
                }
            }
        }
    }
}
