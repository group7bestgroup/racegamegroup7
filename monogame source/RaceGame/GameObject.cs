﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    public abstract class GameObject
    {
        /// <summary>
        /// gets called at he start of the game
        /// </summary>
        /// <param name="content">ContentManager</param>
        public abstract void Start(ContentManager content);

        /// <summary>
        /// gets called every frame
        /// </summary>
        /// <param name="delta">deltatime</param>
        public abstract void Update(float delta);

        /// <summary>
        /// used for drawing the sprites of the object
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch</param>
        public abstract void Draw(SpriteBatch spriteBatch);

        /// <summary>
        /// returns the collision box of the gameobject
        /// </summary>
        public virtual Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X - (sprite.Width / 2), (int)position.Y - (sprite.Height / 2), sprite.Width, sprite.Height);
        }

        /// <summary>
        /// gets called when a other object collides with this object
        /// </summary>
        /// <param name="other">the object it collided with</param>
        public abstract void OnCollisionEnter(GameObject other);

        /// <summary>
        /// used to draw the debug sprites
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void DebugDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(boundingSprite, GetBoundingBox(), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        /// <summary>
        /// if a object can colide with it
        /// </summary>
        public bool colidable = false;

        /// <summary>
        /// the objects main sprite
        /// </summary>
        public Texture2D sprite;

        /// <summary>
        /// the sprite used for collision
        /// </summary>
        public Texture2D boundingSprite;
        
        /// <summary>
        /// the objects postion
        /// </summary>
        public Vector2 position;

        /// <summary>
        /// the objecs rotation 
        /// </summary>
        public float rotation;

        /// <summary>
        /// the collision box
        /// </summary>
        public Rectangle BoundingBox;
    }
}