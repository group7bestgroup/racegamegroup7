﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    public class Terrain : GameObject
    {
        private string image;
        private bool touched;
        private Vector2 size;

        /// <summary>
        /// Creating static terrain
        /// </summary>
        /// <param name="img">the name of the sprite</param>
        /// <param name="x">the position along the X axis</param>
        /// <param name="y">the position along the Y axis</param>
        /// <param name="rot">the rotation of the object</param>
        public Terrain(string img, int x, int y, float width, float height, float rot)
        {
            image = img;
            position.X = x;
            position.Y = y;
            rotation = rot;
            colidable = true;
            size.X = width;
            size.Y = height;

        }

        public override void Start(ContentManager content)
        {

            //checks if the sprites is null
            if (image != null)
                sprite = content.Load<Texture2D>("Assets/Images/Terrain/" + image);
            else
                sprite = null;
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");


        }

        public override void Update(float delta)
        {

        }

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        public override void DebugDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(boundingSprite, GetBoundingBox(), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (sprite != null)
                spriteBatch.Draw(sprite, GetBoundingBox(), null, Color.Red, rotation, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        public override void OnCollisionEnter(GameObject other)
        {
            touched = true;
        }
    }
}
