﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    /// <summary>
    /// Contains the checkpoint behaviour
    /// </summary>
    class Checkpoint : GameObject
    {
        //size of the checkpoint
        private Vector2 size;

        //id of the checkpoint
        private int checkPointId;

        private RaceLevel levelManager;

        /// <summary>
        /// creates a new checkpoint
        /// </summary>
        /// <param name="pos">position of the checkpoint</param>
        /// <param name="size">size of the checkpoint</param>
        /// <param name="spr">sprite of the checkpoint</param>
        /// <param name="id">id of the checkpoint</param>
        /// <param name="gm">gamemanager(racelevel component)</param>
        public Checkpoint(Vector2 pos, Vector2 size, Texture2D spr, int id, RaceLevel gm)
        {
            this.position = pos;
            this.size = size;
            this.sprite = spr;
            this.checkPointId = id;
            this.levelManager = gm;
        }

        //start behaviour
        public override void Start(ContentManager content)
        {
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");
        }

        //update behaviour
        public override void Update(float delta) { }

        //drawing behaviour
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, new Rectangle(Convert.ToInt32(position.X), Convert.ToInt32(position.Y), Convert.ToInt32(size.X), Convert.ToInt32(size.Y)),
                                                            null, Color.White, rotation, new Vector2(sprite.Width / 2, sprite.Height / 2), SpriteEffects.None, 1);
        }

        //returns bounding box of the checkpoint
        public override Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        //draws size of checkpoint when debugdrawing is enabled
        public override void DebugDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(boundingSprite, GetBoundingBox(), null, Color.Green, 0, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        /// <summary>
        /// Checks collision with this object
        /// </summary>
        /// <param name="other">object colliding with</param>
        public override void OnCollisionEnter(GameObject other)
        {
            if (other.GetType() == typeof(Vehicle))
            {
                Vehicle otherVehicle = (Vehicle)other;

                if (otherVehicle.GetCheckPoints() == this.checkPointId - 1)
                {
                    otherVehicle.SetCheckPoints(otherVehicle.GetCheckPoints() + 1);
                }
                if (this.checkPointId == 0)
                {
                    if (otherVehicle.GetCheckPoints() == levelManager.GetTotalCheckPoints() - 1)
                    {
                        if (otherVehicle.GetLaps() != otherVehicle.GetMaxLaps())
                        {
                            otherVehicle.SetLaps(otherVehicle.GetLaps() + 1);
                            otherVehicle.SetPassedPitstop(false);
                            otherVehicle.SetCheckPoints(0);
                        }
                    }
                }
            }
        }

    }
}
