﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms.VisualStyles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace RaceGame
{
    /// <summary>
    /// Represents the vehicle that the player uses to race.
    /// @author Jan Julius de Lang
    /// @author Arthur van den Broek
    /// @author Wesley Hofma
    /// </summary>
    class Vehicle : GameObject
    {
        //appearance and properties of the vehicle
        private float speed;
        private int playerId;
        private string name;

        //keys to control the vehicle
        private Keys keyFwd, keyRight, keyBck, keyLeft, actionKey;

        //properties that add extra gameplay
        private float fuel;
        private int health;
        private const int maxHealth = 100;
        private const int maxFuel = 100;
        private const int maxSpeed = 3;
        private const int maxLaps = 3;

        private int pitStopCounter;
        private int laps;
        private int checkPoints;
        private int ranking;
        private int rankingWeight;
        private bool hasPassedPitstop;
        private bool finished;

        bool hasCollidedRecently = false;
        private float finishedTime;

        //speed of the rotation the vehicle goes at
        private float rotationSpeed = 1.5f;
        //how fast the vehicle goes
        private float moveSpeed = 1f;
        //how fast the vehicle goes backwards
        private float backwardSpeed = 0.5f;

        public bool isInSpeedup = false;
        bool hasBomb = false;

        private RaceLevel levelManager;

        private Texture2D boundingBox;

        private PlayerStats myStats;

        private int damageStackingCounter = 0;

        private float qTimer = 0;


        /// <summary>
        /// Initializing the vehicle with variables
        /// </summary>
        /// <param name="positionx">Start position on the x axis.</param>
        /// <param name="positiony">Start position on the y axis.</param>
        /// <param name="rotation">Start rotation 0-360</param>
        /// <param name="speed">Start speed.</param>
        /// <param name="fuel">Start fuel.</param>
        /// <param name="health">Start health.</param>
        /// <param name="game">a reference to the gamemanager</param>
        /// <param name="keyFwd">ForwardKey</param>
        /// <param name="keyRight">Right key</param>
        /// <param name="keyBck">Key back</param>
        /// <param name="keyLeft">Key left</param>
        /// <param name="sprite">Vehicle sprite</param>
        public Vehicle(int positionx, int positiony, float rotation, float speed,
                        int fuel, int health, int playerid, RaceLevel game,
                        Keys keyFwd, Keys keyRight,
                        Keys keyBck, Keys keyLeft,
                        Keys actionKey
                        )
        {
            position.X = positionx;
            position.Y = positiony;
            this.speed = speed;
            this.rotation = rotation;
            this.fuel = fuel;
            this.health = health;
            this.keyFwd = keyFwd;
            this.keyBck = keyBck;
            this.keyRight = keyRight;
            this.keyLeft = keyLeft;
            this.actionKey = actionKey;
            this.playerId = playerid;
            this.levelManager = game;
        }

        /// <summary>
        /// Returns the rotation speed of the car
        /// </summary>
        /// <returns>Rotationspeed of the vehicle</returns>
        public float getRotationSpeed()
        {
            return this.rotationSpeed;
        }

        /// <summary>
        /// Sets the vehicles rotation speed
        /// </summary>
        /// <param name="rotSpeed">rotation speed in ints per 360 deg</param>
        public void setRotationSpeed(int rotSpeed)
        {
            this.rotationSpeed = rotSpeed;
        }

        public override void Start(ContentManager content)
        {
            sprite = content.Load<Texture2D>("Assets/Images/Vehicle/vehicle" + playerId);
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");
        }

        /// <summary>
        /// Updates the vehicle every tick
        /// </summary>
        public override void Update(float delta)
        {
            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(keyRight))
            {
                if (speed >= 0.1f || speed <= -0.1f)
                {
                    rotation += rotationSpeed * delta;
                }
            }

            if (state.IsKeyDown(keyLeft))
            {
                if (speed >= 0.1f || speed <= -0.1f)
                {
                    rotation += -rotationSpeed * delta;
                }
            }
            if (isInSpeedup)
            {
                if (state.IsKeyDown(keyFwd))
                {
                    if (speed >= 3)
                    {
                        StopEntityAuto(delta);
                    }
                }

                if (state.IsKeyDown(keyBck))
                {
                    speed += -moveSpeed * delta;
                    StopEntity(delta);
                }


                if (state.IsKeyUp(keyBck) && state.IsKeyUp(keyFwd))
                {
                    StopEntityAuto(delta);
                }
            }
            else
            {
                if (state.IsKeyDown(keyFwd))
                {
                    if (speed <= 3)
                    {
                        speed += moveSpeed * delta;
                    }
                }

                if (state.IsKeyDown(keyBck))
                {
                    StopEntity(delta);
                }


                if (state.IsKeyUp(keyBck) && state.IsKeyUp(keyFwd))
                {
                    StopEntityAuto(delta);
                }
            }


            if (state.IsKeyDown(actionKey))
            {
                SetHealth(GetHealth() -1);
                if (GetBomb())
                {
                    Powerup bomb = new Powerup(Powerup.PowerupType.Bomb, position, 0, false,
                        levelManager.gameManager.PowerUpSprites[(int)Powerup.PowerupType.Bomb], levelManager.gameManager.defaultBoundingBox, playerId, false);

                    levelManager.gameManager.CreateGameObject(bomb);
                    SetBomb(false);
                }
            }

            //terrain collision
            foreach (GameObject gObject in levelManager.gameManager.GameObjects)
            {
                if (GetBoundingBox().Intersects(gObject.GetBoundingBox()))
                {
                    if (gObject.colidable)
                    {
                        isInSpeedup = false;
                        if (!hasCollidedRecently)
                        {
                            if (speed >= 0)
                            {
                                SetSpeed((-GetSpeed()+GetSpeed() + 0.5f));
                                SetHealth(Convert.ToInt32(GetHealth()-5 * damageStackingCounter *1.2f));
                                hasCollidedRecently = true;
                                damageStackingCounter++;
                                qTimer = 0;
                            }
                            else
                            {
                                speed = (GetSpeed() + 1f);
                                hasCollidedRecently = true;
                                SetHealth(Convert.ToInt32(GetHealth() - 5 * damageStackingCounter * 1.2f));
                                damageStackingCounter++;
                                qTimer = 0;
                            }
                        }
                        else
                        {
                            qTimer += 1*delta;
                            Debug.WriteLine(qTimer.ToString());
                            if (qTimer > 0.5f)
                            {
                                hasCollidedRecently = false;
                            }
                        }
                    }


                    gObject.OnCollisionEnter(this);
                }
            }

            UpdatePosition();
            MoveEntity(speed, delta);
            DepleteFuel();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, new Rectangle(Convert.ToInt32(position.X),
                                                            Convert.ToInt32(position.Y), sprite.Width, sprite.Height),
                                                            null, Color.White, rotation, new Vector2(sprite.Width / 2, sprite.Height / 2), SpriteEffects.None, 1);
        }

        public override void OnCollisionEnter(GameObject other)
        {

        }

        /// <summary>
        /// moves the vehicle
        /// </summary>
        /// <param name="speed">speed at which it moves</param>
        public void MoveEntity(float speed, float delta)
        {
            Vector2 direction = new Vector2((float)Math.Cos(rotation),
                                            (float)Math.Sin(rotation));
            direction.Normalize();
            position += direction * speed;
        }

        /// <summary>
        /// stops the vehicle with button
        /// </summary>
        /// <param name="delta"></param>
        public void StopEntity(float delta)
        {
            if (speed > 0)
            {
                speed -= (moveSpeed * 2) * delta;
            }
            if (speed < 0)
            {
                speed -= (moveSpeed / 2) * delta;
            }

            speed = MathHelper.Clamp(speed, -maxSpeed, maxSpeed);
        }

        /// <summary>
        /// stops the entity automatically
        /// </summary>
        /// <param name="delta">delta time</param>
        public void StopEntityAuto(float delta)
        {
            if (speed >= 0.1f || speed <= -0.1f)
            {
                if (speed < 0)
                {
                    if (speed < 0.1f)
                    {
                        speed += 0.5f * delta;
                    }
                }
                if (speed > 0 && speed < 3.1f)
                {
                    isInSpeedup = false;
                    speed -= 1.8f * delta;
                    if (speed < 0.1f)
                    {
                        speed = 0;
                    }
                }
                if (speed > 3.1f)
                {
                    speed -= 1.2f * delta;
                }
            }
        }

        /// <summary>
        /// deplete fuel
        /// </summary>
        public void DepleteFuel()
        {
            if (speed > 0)
            {
                fuel -= 0.04f;
            }
            else if (speed > 1.5f)
            {
                fuel -= 0.09f;
            }
            else if (speed > 2.5f)
            {
                fuel -= 0.13f;
            }
        }

        /// <summary>
        /// Toggles the speedup mode
        /// </summary>
        /// <param name="speedUpSpeed">how fast the vehicle will go</param>
        public void ToggleSpeedUp(float speedUpSpeed)
        {
            speed = speedUpSpeed;
            isInSpeedup = true;
        }

        void UpdatePosition()
        {
            rankingWeight = Convert.ToInt32((GetLaps() * -1000000) + (GetCheckPoints() * -10000) + GetDistanceToNext());
        }

        /// <summary>
        /// gives distance to the next checkpoint
        /// </summary>
        /// <returns>distance to next checkpoint</returns>
        public float GetDistanceToNext()
        {
            if (GetCheckPoints() == levelManager.GetTotalCheckPoints() - 1)
            {
                return Vector2.Distance(position, levelManager.GetNextCheckPointPosition(0));
            }
            return Vector2.Distance(position, levelManager.GetNextCheckPointPosition(GetCheckPoints() + 1));
        }

        /// <summary>
        /// set the current vehicles fuel
        /// </summary>
        /// <param name="f">amount of fuel</param>
        public void SetFuel(float f)
        {
            fuel = f;
            if (fuel < 0)
            {
                Die();
            }

            if (f >= maxFuel)
            {
                fuel = maxFuel;
            }
        }

        /// <summary>
        /// get current vehicle fuel
        /// </summary>
        /// <returns>fuel</returns>
        public float GetFuel()
        {
            return fuel;
        }

        /// <summary>
        /// set current vehicle hp 
        /// </summary>
        /// <param name="hp">set to this value</param>
        public void SetHealth(int hp)
        {
            health = hp;
            if (health <= 0)
            {
                Die();
            }

            if (health >= maxHealth)
            {
                health = maxHealth;
            }
        }

        /// <summary>
        /// get current vehicle health
        /// </summary>
        /// <returns>health</returns>
        public int GetHealth()
        {
            return health;
        }

        /// <summary>
        /// When the player dies!
        /// </summary>
        public void Die()
        {
            ExecutePitstop();
            Respawn(levelManager.GetLastCheckpointPosition(GetCheckPoints()), levelManager.GetLastCheckpointRotation(GetCheckPoints()));
        }

        /// <summary>
        /// respawn the vehicle 
        /// </summary>
        /// <param name="pos">position</param>
        /// <param name="rot">rotation</param>
        public void Respawn(Vector2 pos, float rot)
        {
            this.position = pos;
            this.rotation = Util.DegreeToRadian(rot);
            isInSpeedup = false;
            SetSpeed(0);
        }

        /// <summary>
        /// set current position to position
        /// </summary>
        /// <param name="pos">x and y value for position</param>
        public void SetPosition(Vector2 pos)
        {
            position = pos;
        }

        /// <summary>
        /// returns position of current vehicle
        /// </summary>
        /// <returns>position</returns>
        public Vector2 GetPosition()
        {
            return position;
        }

        /// <summary>
        /// set speed of current vehicle
        /// </summary>
        /// <param name="sp">value to set speed to</param>
        public void SetSpeed(float sp)
        {
            speed = sp;
        }

        /// <summary>
        /// returns current vehicle speed
        /// </summary>
        /// <returns>speed</returns>
        public float GetSpeed()
        {
            return speed;
        }

        /// <summary>
        /// returns true if the player has a bomb
        /// </summary>
        /// <returns>hasbomb</returns>
        public bool GetBomb()
        {
            return hasBomb;
        }

        /// <summary>
        /// set to true if player has a bomb and to false if player does not have a bomb
        /// </summary>
        /// <param name="b">has bomb?</param>
        public void SetBomb(bool b)
        {
            hasBomb = b;
        }

        /// <summary>
        /// returns playerid of the player
        /// </summary>
        /// <returns>playerid</returns>
        public int GetPlayerId()
        {
            return playerId;
        }

        /// <summary>
        /// returns the maximum fuel 
        /// </summary>
        /// <returns></returns>
        public int GetMaxFuel()
        {
            return maxFuel;
        }

        /// <summary>
        /// returns
        /// </summary>
        /// <returns></returns>
        public int GetMaxHealth()
        {
            return maxHealth;
        }

        /// <summary>
        /// returns laps of vehicle
        /// </summary>
        /// <returns>current vehicle laps</returns>
        public int GetLaps()
        {
            return laps;
        }

        /// <summary>
        /// set the laps of current vehicle
        /// </summary>
        /// <param name="l"></param>
        public void SetLaps(int l)
        {
            if (l == maxLaps)
            {
                int bigNumber = 999999999;
                SetRankingWeight(GetRankingWeight() + bigNumber / (ranking+1));
                SetFinished(true);
            }
            laps = l;
        }

        /// <summary>
        /// returns current checkpoints counter
        /// </summary>
        /// <returns>amount of checkpoints</returns>
        public int GetCheckPoints()
        {
            return checkPoints;
        }

        /// <summary>
        /// sets checkpoint counter
        /// </summary>
        /// <param name="c">value</param>
        public void SetCheckPoints(int c)
        {
            checkPoints = c;
        }

        /// <summary>
        /// returns current ranking
        /// </summary>
        /// <returns>ranking</returns>
        public int GetRanking()
        {
            return ranking;
        }

        /// <summary>
        /// sets the current ranking 
        /// </summary>
        /// <param name="r">value</param>
        public void SetRanking(int r)
        {
            ranking = r;
        }

        /// <summary>
        /// returns ranking weight
        /// </summary>
        /// <returns>rankingweight</returns>
        public int GetRankingWeight()
        {
            return rankingWeight;
        }

        public void SetRankingWeight(int c)
        {
            rankingWeight = c;
        }

        /// <summary>
        /// returns how many pitstops were used
        /// </summary>
        /// <returns>pitstopcounter</returns>
        public int GetPitstopCounter()
        {
            return pitStopCounter;
        }

        /// <summary>
        /// sets pitstop counter
        /// </summary>
        /// <param name="c">value</param>
        public void SetPitstopCounter(int c)
        {
            pitStopCounter = c;
        }

        /// <summary>
        /// returns if the player has recently passed a pitstop
        /// </summary>
        /// <returns>bool</returns>
        public bool GetPassedPitstop()
        {
            return hasPassedPitstop;
        }

        /// <summary>
        /// sets if vehicle recently passed pitstop
        /// </summary>
        /// <param name="p">has passed</param>
        public void SetPassedPitstop(bool p)
        {
            hasPassedPitstop = p;
        }

        /// <summary>
        /// runs what has to happen when player passes pitstop
        /// </summary>
        public void ExecutePitstop()
        {
            SetHealth(GetMaxHealth());
            SetFuel(GetMaxFuel());
            SetPitstopCounter(GetPitstopCounter() + 1);
            SetDamageStackingCounter(0);
            SetPassedPitstop(true);
        }
        /// <summary>
        /// executes when the player finished
        /// </summary>
        public void SetFinished(bool f)
        {
            if (f)
            {
                SetFinishedTime(myStats.GetTimer());
            }
            finished = f;
        }

        /// <summary>
        /// returns value if the player finished or not
        /// </summary>
        /// <returns></returns>
        public bool GetFinished()
        {
            return finished;
        }

        /// <summary>
        /// sets finished time
        /// </summary>
        /// <param name="t"></param>
        public void SetFinishedTime(float t)
        {
            finishedTime = t;
        }

        /// <summary>
        /// returns finished time
        /// </summary>
        /// <returns></returns>
        public float GetFinishedTime()
        {
            return finishedTime;
        }

        /// <summary>
        /// sets player stats to playerstats class
        /// </summary>
        /// <param name="p"></param>
        public void SetPlayerStats(PlayerStats p)
        {
            myStats = p;
        }

        /// <summary>
        /// set damage stacking counter to int value
        /// </summary>
        /// <param name="c"></param>
        public void SetDamageStackingCounter(int c)
        {
            damageStackingCounter = c;
        }

        /// <summary>
        /// return current damage stacking 
        /// </summary>
        /// <returns></returns>
        public int GetDamageStackingCounter()
        {
            return damageStackingCounter;
        }

        /// <summary>
        /// returns max laps
        /// </summary>
        /// <returns></returns>
        public int GetMaxLaps()
        {
            return maxLaps;
        }
    }
}
