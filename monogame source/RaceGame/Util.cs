﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceGame
{
    /// <summary>
    /// contains handy methods :)
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// returns conversion from degrees to radians 
        /// </summary>
        /// <param name="deg">degrees</param>
        /// <returns>radians</returns>
        public static float DegreeToRadian(float deg)
        {
            return Convert.ToSingle(Math.PI * deg / 180.0);
        }

        /// <summary>
        /// converts a float to a "timer"
        /// </summary>
        /// <param name="time">float to convert to timer</param>
        /// <returns>mins:seconds.mil</returns>
        public static string ConvertFloatToTimer(float time)
        {
            string minutes = Math.Floor(time / 60).ToString("0");
            return minutes + ":" + (time % 60).ToString("00.0");
        }

        /// <summary>
        /// returns the difference between 2 values
        /// </summary>
        /// <param name="nr1">value 1</param>
        /// <param name="nr2">value 2</param>
        /// <returns>differnce between value 1 & value 2</returns>
        public static float FindDifference(float nr1, float nr2)
        {
            return Math.Abs(nr1 - nr2);
        }
    }
}
