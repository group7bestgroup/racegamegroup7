﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RaceGame
{
    class ExitButton : GameObject
    {

        private Color color;
        private Game1 gameManager;
        private MouseState oldState;

        public ExitButton(Texture2D sprite, Vector2 position, Game1 gm)
        {
            this.sprite = sprite;
            this.position = position;
            gameManager = gm;
        }

        public override void Start(ContentManager content)
        {
            boundingSprite = content.Load<Texture2D>("Assets/Images/Terrain/testWall");
        }

        public override void Update(float delta)
        {
            //when the mouse enters the hitbox from teh button change color the red
            if (GetBoundingBox().Contains(new Vector2(Mouse.GetState().X, Mouse.GetState().Y)))
            {
                //if the user presses left mouse button exit te game
                if (Mouse.GetState().LeftButton == ButtonState.Pressed && oldState.LeftButton == ButtonState.Pressed)
                {
                    gameManager.Exit();
                }

                color = Color.Red;
            }
            else
            {
                color = Color.White;
            }

            oldState = Mouse.GetState();
        }

        public override Rectangle GetBoundingBox()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)sprite.Width, (int)sprite.Height);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, GetBoundingBox(), null, color, rotation, new Vector2(0, 0), SpriteEffects.None, 1);
        }

        public override void OnCollisionEnter(GameObject other)
        {
        }
    }
}
