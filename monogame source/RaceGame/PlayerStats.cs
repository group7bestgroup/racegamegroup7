﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    class PlayerStats : GameObject
    {
        private Vehicle myVehicle;
        private RaceLevel level;

        private float timer;
        private char dirChar;

        public Texture2D playerInfoBoxPointer;
        public Texture2D HealthBar;
        public Texture2D[] bombAvailable = new Texture2D[2];

        private readonly Vector2[] Offset =
        {
            new Vector2(-35, -33),
            new Vector2(10, 45),
            new Vector2(0, -20),
            new Vector2(-9, -65),
            new Vector2(-9, -89),
            new Vector2(0,55),
            new Vector2(45,45),
            new Vector2(-45, 50), 
        };

        private readonly int POINTER = 0, CHAR = 1, TIMER = 2, HEALTH = 3, FUEL = 4, PLAYER_SPRITE = 5, PITSTOP_COUNTER = 6, BOMB = 7;

        /// <summary>
        /// Loads the playerstats of a vehicle
        /// </summary>
        /// <param name="pos">position of the playerstat interface</param>
        /// <param name="rot">rotatoin of the playerstat interface</param>
        /// <param name="vehicle">the vehicle the interface belongs to</param>
        /// <param name="l">this racelevel</param>
        public PlayerStats(Vector2 pos, float rot, Vehicle vehicle, RaceLevel l)
        {
            this.position = pos;
            this.rotation = rot;
            this.myVehicle = vehicle;
            level = l;
        }

        public override void Start(ContentManager content)
        {
            sprite = content.Load<Texture2D>("Assets/Images/Interface/interface0");
            playerInfoBoxPointer = content.Load<Texture2D>("Assets/Images/Interface/interface1");
            HealthBar = content.Load<Texture2D>("Assets/Images/Interface/interface2");
            bombAvailable[0] = content.Load<Texture2D>("Assets/Images/Interface/interface3");
            bombAvailable[1] = content.Load<Texture2D>("Assets/Images/Interface/interface4");
            myVehicle.SetPlayerStats(this);
        }

        public override void Update(float delta)
        {
            timer += delta;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //bg sprite
            spriteBatch.Draw(sprite, new Rectangle(-Convert.ToInt32(position.X), -Convert.ToInt32(position.Y), sprite.Width, sprite.Height), null, Color.White, 0,
                    new Vector2(0, 0), SpriteEffects.None, 1);

            //speedopointer
            spriteBatch.Draw(playerInfoBoxPointer, new Rectangle(-Convert.ToInt32(position.X + Offset[POINTER].X), -Convert.ToInt32(position.Y + Offset[POINTER].Y), playerInfoBoxPointer.Width, playerInfoBoxPointer.Height), null, Color.White,
                Util.DegreeToRadian(ConvertSpeedToDegrees()),
                    new Vector2(19, 1), SpriteEffects.None, 1);

            //character d/r fowrad backward
            spriteBatch.DrawString(level.gameManager.PixelFont, GetDirectionChar().ToString(), -position + Offset[CHAR],
                Color.Black);

            //timer
            if (!myVehicle.GetFinished())
            {
                spriteBatch.DrawString(level.gameManager.PixelFont, Util.ConvertFloatToTimer(timer), -position + Offset[TIMER],
                    Color.White);
            }
            else
            {
                spriteBatch.DrawString(level.gameManager.PixelFont, Util.ConvertFloatToTimer(myVehicle.GetFinishedTime()), -position + Offset[TIMER],
                    Color.Lime);
            }

            //healthbar
            spriteBatch.Draw(HealthBar, new Rectangle(-Convert.ToInt32(position.X + Offset[HEALTH].X),
                -Convert.ToInt32(position.Y + Offset[HEALTH].Y), myVehicle.GetHealth() / 2, 10), new Rectangle((int)position.X, (int)position.Y, HealthBar.Width, 10), Color.Green);

            //fuelbar
            spriteBatch.Draw(HealthBar, new Rectangle(-Convert.ToInt32(position.X + Offset[FUEL].X),
                -Convert.ToInt32(position.Y + Offset[FUEL].Y), Convert.ToInt32(myVehicle.GetFuel()) / 2, 10), new Rectangle((int)position.X, (int)position.Y, HealthBar.Width, 10), Color.Yellow);

            //vehiclesprite
            spriteBatch.Draw(myVehicle.sprite, new Rectangle(-Convert.ToInt32(position.X + Offset[PLAYER_SPRITE].X), -Convert.ToInt32(position.Y + Offset[PLAYER_SPRITE].Y), myVehicle.sprite.Width, myVehicle.sprite.Height), null, Color.White,
                Util.DegreeToRadian(0),
        new Vector2(0, 0), SpriteEffects.None, 1);

            //pitstopcounter
            spriteBatch.DrawString(level.gameManager.PixelFont, myVehicle.GetPitstopCounter().ToString(), -position + Offset[PITSTOP_COUNTER],
                Color.Black);

            //bomb interface
           spriteBatch.Draw(myVehicle.GetBomb() ? bombAvailable[0] : bombAvailable[1], new Rectangle(-Convert.ToInt32(position.X + Offset[BOMB].X), -Convert.ToInt32(position.Y + Offset[BOMB].Y), myVehicle.sprite.Width, myVehicle.sprite.Height), null, Color.White,
                    Util.DegreeToRadian(0),new Vector2(0, 0), SpriteEffects.None, 1);
            
        }

        public override void DebugDraw(SpriteBatch spriteBatch)
        {

        }

        public override void OnCollisionEnter(GameObject other) { }

        /// <summary>
        /// returns conversion of speed to degrees for speedometer
        /// </summary>
        /// <returns></returns>
        public float ConvertSpeedToDegrees()
        {
            if (myVehicle.GetSpeed() > 0)
            {
                return myVehicle.GetSpeed() * 35;
            }
            else
            {
                return myVehicle.GetSpeed() * -35;
            }
        }

        /// <summary>
        /// returns char based on speed
        /// </summary>
        /// <returns></returns>
        public char GetDirectionChar()
        {
            if (myVehicle.GetSpeed() >= 0.1f)
            {
                return 'D';
            }
            if (myVehicle.GetSpeed() <= 0.1f)
            {
                return 'R';
            }
            if (myVehicle.GetSpeed() >= -0.1f && myVehicle.GetSpeed() <= 0.1f)
            {
                return 'P';
            }
            return 'P';
        }

        /// <summary>
        /// return timer float
        /// </summary>
        /// <returns></returns>
        public float GetTimer()
        {
            return timer;
        }


    }
}
