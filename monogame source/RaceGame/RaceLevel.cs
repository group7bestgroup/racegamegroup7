﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RaceGame
{
    class RaceLevel : Level
    {

        public int playerCount;
        public Game1 gameManager;

        //readonly vars
        private readonly int checkPointCounter = 9;
        private readonly Vector2 standardCheckPointSize = new Vector2(150, 30);
        private readonly Vector2 pitstopPos = new Vector2(750, 170);
        private readonly Vector2 pitstopSize = new Vector2(50, 50);
        private Texture2D backgroundImage;
        private KeyboardState oldState;

        public Vehicle[] vehicles;
        public List<GameObject> CheckpointList = new List<GameObject>();
        private List<Vehicle> playerRanking = new List<Vehicle>();

        #region checkpoints
        private readonly Vector2[] playerSpawnPosition =
        {
            new Vector2(950, 250),
            new Vector2(900,270),
            new Vector2(950,300),
            new Vector2(900,330)
        };

        private readonly float[] playerSpawnRotations =
        {
            90, 90, 90, 90
        };

        public readonly Vector2[] playerInterfacePosition =
        {
            new Vector2(-50, -620),
            new Vector2(-150, -620),
            new Vector2(-250, -620),
            new Vector2(-350, -620)
        };

        private readonly Vector2[] checkPointStartPosition =
            {
                new Vector2(830, 300),//1
                new Vector2(830, 550),//2
                new Vector2(750, 625),//3
                new Vector2(520, 550),//4
                new Vector2(470, 310),//5
                new Vector2(230, 310),//6
                new Vector2(10, 200),//7
                new Vector2(230, -20),//8
                new Vector2(750, -20),//9
            };
        private readonly Vector2[] checkPointStartSize =
            {
                new Vector2(170,32),//1
                new Vector2(170,32),//2
                new Vector2(32,170),//3
                new Vector2(170,32),//4
                new Vector2(32,170),//5
                new Vector2(32,170),//6
                new Vector2(170,32),//7
                new Vector2(32,170),//8
                new Vector2(32,240),//9

            };

        private readonly float[] checkPointPlayerRotation =
        {
            90, //1
            90, //2
            180, //3
            270, //4
            180, //5
            180, //6
            270, //7
            0, //8
            0, //9
        };
        #endregion

        /// <summary>
        /// starts a new racelevel
        /// </summary>
        /// <param name="playerCount">amount of players</param>
        /// <param name="gameManager">game1 reference</param>
        public RaceLevel(int playerCount, Game1 gameManager)
        {
            this.playerCount = playerCount;
            this.gameManager = gameManager;
        }

        public override void Start(ContentManager content)
        {

            backgroundImage = gameManager.Content.Load<Texture2D>("Assets/Images/Backgrounds/background0");

            //set lenght of array
            vehicles = new Vehicle[playerCount];

            //initializing the vehicles(players)
            for (int i = 0; i < playerCount; i++)
            {
                gameManager.GameObjects.Add(new Vehicle(Convert.ToInt32(playerSpawnPosition[i].X), Convert.ToInt32(playerSpawnPosition[i].Y), Util.DegreeToRadian(playerSpawnRotations[i]), 0, 100, 100, i, this,
                   gameManager.playerKeys[i, 0], gameManager.playerKeys[i, 1], gameManager.playerKeys[i, 2], gameManager.playerKeys[i, 3], gameManager.playerKeys[i, 4]));
                vehicles[i] = (Vehicle)gameManager.GameObjects[gameManager.GameObjects.Count - 1];
                playerRanking.Add(vehicles[i]);
                gameManager.GameObjects.Add(new PlayerStats(playerInterfacePosition[i], 0, vehicles[i], this));
            }

            //adding the sprites from the powerupsprites
            for (int i = 0; i < 8; i++)
            {
                gameManager.PowerUpSprites.Add(gameManager.Content.Load<Texture2D>("Assets/Images/Powerups/" + i));
            }

            //spawns all the powerups
            #region powerups
            Pitstop pitstop = new Pitstop(pitstopPos, 0, pitstopSize);

            Powerup powerup0 = new Powerup(Powerup.PowerupType.SpeedBoosts, new Vector2(400, 425), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.SpeedBoosts], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup1 = new Powerup(Powerup.PowerupType.FuelReplenish, new Vector2(700, 650), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.FuelReplenish], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup2 = new Powerup(Powerup.PowerupType.Heal, new Vector2(475, 50), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.Heal], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup3 = new Powerup(Powerup.PowerupType.PickupBomb, new Vector2(85, 210), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.PickupBomb], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup4 = new Powerup(Powerup.PowerupType.SpeedBoosts, new Vector2(920, 425), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.SpeedBoosts], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup5 = new Powerup(Powerup.PowerupType.PickupBomb, new Vector2(878, 100), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.PickupBomb], gameManager.defaultBoundingBox, -1, true);

            Powerup powerup6 = new Powerup(Powerup.PowerupType.Heal, new Vector2(850, 650), 0, false,
                gameManager.PowerUpSprites[(int)Powerup.PowerupType.Heal], gameManager.defaultBoundingBox, -1, true);

            gameManager.GameObjects.Add(powerup0);
            gameManager.GameObjects.Add(powerup1);
            gameManager.GameObjects.Add(powerup2);
            gameManager.GameObjects.Add(powerup3);
            gameManager.GameObjects.Add(powerup4);
            gameManager.GameObjects.Add(powerup5);
            gameManager.GameObjects.Add(powerup6);
            #endregion

            gameManager.GameObjects.Add(pitstop);



            for (int i = 0; i < checkPointCounter; i++)
            {
                Checkpoint[] checkpoints = new Checkpoint[checkPointCounter];
                checkpoints[i] = new Checkpoint(checkPointStartPosition[i], checkPointStartSize[i],
                    gameManager.Content.Load<Texture2D>("Assets/Images/Terrain/checkpointSprite"), i, this);
                gameManager.GameObjects.Add(checkpoints[i]);
                CheckpointList.Add(checkpoints[i]);
            }

            //spawns every terrain object
            #region terrain objects
            gameManager.GameObjects.Add(new Terrain(null, 190, 145, 90, 160, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 150, 180, 90, 90, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 240, 110, 420, 230, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 175, -44, 700, 50, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 200, 445, 289, 50, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 980, 149, 50, 470, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 980, 149, 50, 470, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 835, 5, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 855, 25, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 875, 45, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 900, 65, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 925, 85, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 945, 105, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 965, 125, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 975, 635, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 960, 650, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 945, 665, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 930, 680, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 915, 695, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 900, 710, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 885, 725, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 870, 740, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 855, 755, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 840, 770, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 680, 760, 200, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 545, 635, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 560, 650, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 575, 665, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 590, 680, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 605, 695, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 620, 710, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 635, 725, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 650, 740, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 665, 755, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 540, 508, 10, 100, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 500, 470, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 515, 485, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 530, 500, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 40, 320, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 55, 335, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 70, 350, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 85, 365, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 100, 380, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 115, 395, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 130, 405, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 145, 420, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 160, 435, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 175, 450, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 30, 145, 10, 165, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 170, 10, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 155, 25, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 135, 40, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 120, 55, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 100, 70, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 80, 85, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 60, 100, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 40, 115, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 565, 350, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 580, 365, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 595, 380, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 610, 395, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 625, 410, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 640, 425, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 655, 440, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 670, 465, 10, 120, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 685, 590, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 700, 605, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 715, 620, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 730, 635, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 745, 650, 35, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 845, 585, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 830, 600, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 815, 615, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 800, 630, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 785, 645, 10, 10, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 860, 270, 10, 310, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 750, 115, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 780, 135, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 805, 165, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 835, 190, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 700, 190, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 735, 220, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 775, 250, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 815, 270, 20, 20, 0f));
            gameManager.GameObjects.Add(new Terrain(null, 0, 500, 530, 500, 0f));
            #endregion

            foreach (GameObject gameObject in gameManager.GameObjects)
            {
                gameObject.Start(content);
            }
        }

        public override void Update(float delta)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) && oldState.IsKeyUp(Keys.Escape))
                gameManager.gamePauzed = !gameManager.gamePauzed;

            oldState = Keyboard.GetState();
            UpdatePlayerRanking();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(backgroundImage, new Rectangle(0, 0, 1024, 768),
                                                null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
            //draws the player rankings
            for (int i = 0; i < playerCount; i++)
            {
                spriteBatch.DrawString(gameManager.PixelFont, "#" + (i + 1) + ": P" + (playerRanking[i].GetPlayerId() + 1 + " " + playerRanking[i].GetLaps() + "/" + playerRanking[i].GetMaxLaps()), new Vector2(1, 400 + (i * 18)), Color.White);
            }
        }

        /// <summary>
        /// returns next position relative to the value given used to calculate distance to next checkpoint
        /// </summary>
        /// <param name="n">current checkpoint</param>
        /// <returns></returns>
        public Vector2 GetNextCheckPointPosition(int n)
        {
            return checkPointStartPosition[n];
        }

        /// <summary>
        /// returns total checkpoints
        /// </summary>
        /// <returns></returns>
        public int GetTotalCheckPoints()
        {
            return checkPointCounter;
        }

        //updates the player rankings
        public void UpdatePlayerRanking()
        {
            for (int i = 0; i < playerCount; i++)
            {
                playerRanking = playerRanking.OrderBy(x => x.GetRankingWeight()).ToList();
            }
        }

        /// <summary>
        /// returns last checkpoint position
        /// </summary>
        /// <param name="currentCheckPoint"></param>
        /// <returns></returns>
        public Vector2 GetLastCheckpointPosition(int currentCheckPoint)
        {
            return CheckpointList[currentCheckPoint].GetBoundingBox().Center.ToVector2();
        }

        /// <summary>
        /// returns last checkpoint rotation
        /// </summary>
        /// <returns></returns>
        public float GetLastCheckpointRotation(int currentCheckPoint)
        {
            return checkPointPlayerRotation[currentCheckPoint];
        }

    }
}
