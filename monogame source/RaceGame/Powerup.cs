﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RaceGame
{
    class Powerup : GameObject
    {
        string imagePath = "Assets/Images/Powerups/";

        public enum PowerupType
        {
            None = 0,
            SpeedBoosts = 1,
            FuelReplenish = 2,
            Heal = 3,
            Bomb = 4,
            PickupBomb = 5
        }

        private PowerupType powerupType;

        private int powerupId = 2;
        private int placedByPlayerId = -1;

        private float rotationSpeed;
        private float respawnTimer;

        private bool spin;
        private bool respawns = false;

        private Texture2D boundingBox;
        private ContentManager cm;

        private Vector2 oldPosition;

        private Game1 gameManager;

        /// <summary>
        /// Creates a powerup object
        /// </summary>
        /// <param name="poweruptype">Tyoe of poewrup type PowerupType for powerup types</param>
        /// <param name="position">Positino of the object in vector2 x y</param>
        /// <param name="startRotation">STart rotation default 0</param>
        /// <param name="spin">if true object spins around own axis</param>
        /// <param name="playerId">Player id of the player that placed the powerup put -1 if not a player</param>
        public Powerup(PowerupType poweruptype, Vector2 position, float startRotation, bool spin, Texture2D sprite, Texture2D boundingbox, int playerId, bool respawns)
        {
            this.powerupType = poweruptype;
            this.position = position;
            this.rotation = startRotation;
            this.spin = spin;
            this.sprite = sprite;
            this.boundingSprite = boundingbox;
            this.placedByPlayerId = playerId;
            this.respawns = respawns;
            this.oldPosition = position;

        }

        /// <summary>
        /// runs at start of object
        /// </summary>
        /// <param name="content"></param>
        public override void Start(ContentManager content)
        {

        }

        /// <summary>
        /// Updates every tick
        /// </summary>
        public override void Update(float delta)
        {
            if (respawns)
            {
                respawnTimer += 1 * delta;

                if (respawnTimer >= 20)
                {
                    position = oldPosition;
                }
            }
        }

        /// <summary>
        /// Draws the sprite
        /// </summary>
        /// <param name="spritebatch"></param>
        public override void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(sprite, new Rectangle(Convert.ToInt32(position.X), Convert.ToInt32(position.Y), sprite.Width, sprite.Height),
                                                            null, Color.White, rotation, new Vector2(sprite.Width / 2, sprite.Height / 2), SpriteEffects.None, 1);

            //spritebatch.Draw(boundingBox, GetBoundingBox(), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);

        }

        public override void OnCollisionEnter(GameObject other)
        {
            if (other.GetType() == typeof(Vehicle))
            {
                Vehicle player = (Vehicle)other;

                int giveFuel = 25;
                int giveHealth = 25;

                switch (powerupType)
                {
                    case PowerupType.None:
                        Exhaust(true);
                        break;

                    case PowerupType.SpeedBoosts:
                        player.ToggleSpeedUp(5);
                        Exhaust(true);

                        break;
                    case PowerupType.Bomb:
                        if (placedByPlayerId != player.GetPlayerId())
                        {
                            player.SetHealth(player.GetHealth() - 50);
                            Exhaust(false);
                        }
                        break;

                    case PowerupType.FuelReplenish:
                        player.SetFuel(player.GetFuel() + giveFuel);
                        Exhaust(true);
                        break;

                    case PowerupType.Heal:
                        player.SetHealth(player.GetHealth() + giveHealth);
                        player.SetDamageStackingCounter(0);
                        Exhaust(true);

                        break;
                    case PowerupType.PickupBomb:
                        player.SetBomb(true);
                        Exhaust(true);
                        break;
                }
            }
        }

        /// <summary>
        /// exhaust the vehicle
        /// </summary>
        /// <param name="respawns">wether it respawns or not</param>
        void Exhaust(bool respawns)
        {
            if (respawns)
            {
                position = new Vector2(10000, 10000);
                respawnTimer = 0;

            }
            else
            {
                position = new Vector2(10000, 10000);
            }
        }
    }
}
